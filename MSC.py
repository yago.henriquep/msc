# [0, 0, 0, 0, 0, 0, 0],
# [0, 0, 0, 0, 1, 1, 1],
# [0, 1, 1, 1, 1, 2, 2],
# [0, 1, 1, 2, 2, 2, 2],
# [0, 1, 1, 2, 2, 3, 3],
# [0, 1, 2, 2, 2, 3, 3],
# [0, 1, 2, 2, 3, 3, 4],
# [0, 1, 2, 2, 3, 4, 4]]

# ['', '', '', '', '', '', ''],
# ['', 'UP', 'UP', 'UP', 'UPLEFT', 'LEFT', 'UPLEFT'],
# ['', 'UPLEFT', 'LEFT', 'LEFT', 'UP', 'UPLEFT', 'LEFT'],
# ['', 'UP', 'UP', 'UPLEFT', 'LEFT', 'UP', 'UP'],
# ['', 'UPLEFT', 'UP', 'UP', 'UP', 'UPLEFT', 'LEFT'],
# ['', 'UP', 'UPLEFT', 'UP', 'UP', 'UP', 'UP'],
# ['', 'UP', 'UP', 'UP', 'UPLEFT', 'UP', 'UPLEFT'],
# ['', 'UPLEFT', 'UP', 'UP', 'UP', 'UPLEFT', 'UP']]

# UP = U
# LEFT = L
# UPLEFT = UL

def MSC_Length(X, Y):
    m = len(X) + 1;
    n = len(Y) + 1; # +1 ajuste python

    #for i in range(m):
    #    c[i][0] = 0;
    #for j in range(n):
    #    c[0][j] = 0;

    c = [ [ 0 for i in range(n) ] for j in range(m) ] # cria "c" preenchido com 0
    b = [ [ "" for i in range(n) ] for j in range(m) ] #

    for i in range(1, m):
        for j in range(1, n):
            if(X[i - 1] == Y[j - 1]): # -1 ajuste do python
                c[i][j] = c[i - 1][j - 1] + 1;
                b[i][j] = "UL";
            elif( c[i - 1][j] >= c[i][j - 1] ):
                c[i][j] = c[i - 1][j];
                b[i][j] = "U";
            else:
                c[i][j] = c[i][j - 1];
                b[i][j] = "L";
    return c, b;

def MSC_Print(b, X, i, j):
    if( i == 0 or j == 0 ):
        return
    if( b[i][j] == "UL" ):
        MSC_Print(b, X, i - 1, j - 1)
        print( X[i - 1] )
    elif( b[i][j] == "U" ):
        MSC_Print(b, X, i - 1, j)
    else:
        MSC_Print(b, X, i, j - 1)

#X = "ABCBDAB";
#Y = "BDCABA";
X = "ACCGGTCGAGTGCGCGGAAGCCGGCCGAA";
Y = "GTCGTTCGGAATGCCGTTGCTCTGTAAA";

c, b = MSC_Length(X, Y);

print("ENTRADA: ", X, " - ", Y);
print("MSC: ", c[len(c) - 1][len(c[-1]) -1])
print("Solução: ")
MSC_Print(b, X, len(c) - 1, len(c[0]) - 1)
