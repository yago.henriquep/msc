# Identifica a subsequência de um único Vetor
# retorna uma matriz
def subsequencia(V):
    m = len(V);
    #V = bytearray(V, 'utf8')
    subseqMx = [m];

    index = 0;
    for i in range(1 << m):
        subseq = [];
        for j in range(m):
            if( (i & (1 << j)) != 0 ):
                subseq.append(V[j]);
        subseqMx.append( subseq );
        index += 1;

    #return subseqMx;
    for i in range(len(subseqMx)):
        print(subseqMx[i]);

# retorna a Maior Subsequência Comum encontrada com a abordagem Força Bruta.
def MSCFB(X, Y):
    m = len(X); # |X| = m
    n = len(Y); # |Y| = n
    msc = 0;
    mRange = 1 << m;

    for i in range(mRange): # em cada iteração uma sequência é obtida
        CS = [];
        for j in range(m): # obtém a sequência atual
            if( (i & (1 << j)) != 0 ):
                CS.append(X[j]);

        csLength = len(CS);
        subseqCount = 0;
        j = 0;
        # percorre Y comparando a sequência atual
        while (j < n and subseqCount < csLength - 1):
            # caso positivo, igual ao primeiro caractere da subsequência de X
            if(Y[j] == CS[subseqCount]):
                subseqCount += 1; # verificar se o próximo da sequência procede o mesmo
            j += 1;

        if(subseqCount > msc):
            msc = subseqCount;

    return msc;

# Exemplo de entrada
X = "ACCGGTCGAGTGCGCGGAAGCCGGCCGAA";
Y = "GTCGTTCGGAATGCCGTTGCTCTGTAAA";

print("ENTRADA: ", X, " - ", Y);
print( MSCFB(X, Y) )
